package com.oopuniversity.spring.injection.minimal;

import com.oopuniversity.spring.injection.minimal.resources.Injectee;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by OOP University on 6/5/2016.
 */
public class SpringJavaConfigDemo {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.scan("com.oopuniversity.spring.injection.minimal.resources");
        ctx.refresh();
        Injectee injectee = ctx.getBean(Injectee.class);
        injectee.doSomething();
    }


}
